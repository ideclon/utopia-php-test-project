<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Utopia\App;
use Utopia\Request;
use Utopia\Response;
use Utopia\Validator\Wildcard;

use Utopia\Pools\Pool;

use Utopia\Database\Database;
use Utopia\Cache\Cache;
use Utopia\Cache\Adapter\Memory;
use Utopia\Database\Adapter\MariaDB;

$pool = new Pool('mariadb', 1, function() {
    try {
        $pdo = new PDO("mysql:host=127.0.0.1;port=3306;db=utopiatest;charset=utf8mb4;", "root", "mypass");
        $mariadb = new MariaDB($pdo);
        $db = new Database($mariadb, new Cache(new Memory()));

        $db->setDefaultDatabase(
            name: 'utopiaTestProject'
        );
        $db->setNamespace('utopiaTestProject');
    } catch (\PDOException $e) {
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
    }

    return $db;
});

$cache = new Cache(new Memory());

App::setResource('pool', fn () => $pool);
App::setResource('cache', fn () => $cache);

App::error()
    ->inject("error")
    ->inject("response")
    ->action(
        function ($error, $response) {
            $response
                ->setStatusCode(404)
                ->send("Not found - " . $error);
        }
    );

App::get("/")
    ->inject('request')
    ->inject('response')
    ->action(
        function ($request, $response) {
            $response
                ->send('<h1>Go to <a href="/welcome">/welcome</a></h1>');
        }
    );

App::get("/welcome")
    ->inject('request')
    ->inject('response')
    ->action(
        function ($request, $response) {
            $response
                ->json(['DevelopersAreAwesome' => true]);
        }
    );

App::get("/demo")
    ->inject('response')
    ->action(
        function ($response) {
            $response
                ->send("No input sent!");
        }
    );

App::post("/demo/:input")
    ->param("input", "", new Wildcard(), "an input value posted")
    ->inject("response")
    ->action(
        function ($input, $response) {
            $response->json(["response" => "You posted", "input" => $input]);
        }
    );

App::get("/demo/:input")
    ->param("input", "", new Wildcard(), "an input value", true)
    ->inject("response")
    ->action(
        function ($input, $response) {
            $response
                ->send("Your input was " . urldecode($input));
        }
    );

App::get("/db/ping")
    ->inject("response")
    ->inject("pool")
    ->inject("cache")
    ->action(
        function ($response, $pool, $cache) {
            $database = $pool->pop()->getResource();
            // new Database($pools->get($database)->pop()->getResource(), $cache);
            $response
                ->json(["dbList" => $database->ping()]);
        }
    );

App::get("/db/list/")
    ->inject("response")
    ->inject("pool")
    ->inject("cache")
    ->action(
        function ($response, $pool, $cache) {
            $database = $pool->pop()->getResource();
            // new Database($pools->get($database)->pop()->getResource(), $cache);
            $response
                ->json(["collections" => $database->listCollections()]);
        }
    );


App::post("/db/create/:collectionName")
    ->param("collectionName", '', new Wildcard(), 'collection name to create')
    ->inject("response")
    ->inject("pool")
    ->action(
        function ($collectionName, $response, $pool) {
            $database = $pool->pop()->getResource();
            $create = $database->createCollection($collectionName,
                attributes: [],
                indexes: []
            );
            $response
                ->json(['status' => $create]);
        }
    );

App::post("/db/delete/:collectionName")
    ->param("collectionName", '', new Wildcard(), 'collection name to create')
    ->inject("response")
    ->inject("pool")
    ->action(
        function ($collectionName, $response, $pool) {
            $database = $pool->pop()->getResource();
            $create = $database->deleteCollection($collectionName);
            $response
                ->json(['status' => $create]);
        }
    );

App::setMode(App::MODE_TYPE_DEVELOPMENT);

$app = new App("Europe/London");
$request = new Request();
$response = new Response();

$app->run($request, $response);
