# UtopiaPHP test project
I'm trying out [UtopiaPHP](https://github.com/utopia-php), a PHP framework by the [Appwrite](https://github.com/appwrite) team.

This currently supports connecting to a MariaDB server, creating a new database create, delete and list Collections (the equivalent of tables).

## Endpoints
* Create - `POST /db/create/[COLLECTIONNAME]`
* Delete - `POST /db/delete/[COLLECTIONNAME]`
* List - `GET /db/list`

## Setup
The application tries to connect to a MariaDB server on `localhost:3306`, with the credentials `root:mypass`. You can change this at `src/index.php:18`.

As this is just testing, I've been using the PHP built in dev server (`php -S localhost:3000`) to serve the site locally, however the Utopia docs demonstrate using Swoole.